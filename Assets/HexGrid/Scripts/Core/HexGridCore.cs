using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Assertions;
using UnityEngine;

namespace HexGrid.Core
{
    public struct HexCoord
    {
        public int q;
        public int r;
        public int s;

        public const int Even = 1;
        public const int Odd = -1;

        public HexCoord(int q_, int r_, int s_)
        {
            q = q_;
            r = r_;
            s = s_;
            Assert.IsTrue(q + r + s == 0);
        }

        public HexCoord(int q_, int r_)
        {
            q = q_;
            r = r_;
            s = -q - r;
            Assert.IsTrue(q + r + s == 0);
        }

        public static HexCoord Zero()
        {
            return new HexCoord(0, 0, 0);
        }

        // if pointy:  E, NE, NW, W, SW, SE
        // if flat:  SE, NE, N, NW, SW, S  
        static List<HexCoord> direction = new List<HexCoord> {
                new HexCoord(1, 0, -1), new HexCoord(1, -1, 0), new HexCoord(0, -1 ,1),
                new HexCoord(-1, 0, 1), new HexCoord(-1, 1, 0), new HexCoord(0, 1, -1)};
 
        public static HexCoord Direction(int direction_)
        {
            return direction[direction_];
        }

        static List<HexCoord> diagonal = new List<HexCoord> { 
            new HexCoord(2, -1, -1), new HexCoord(1, -2, 1), new HexCoord(-1, -1 ,2),
            new HexCoord(-2, 1, 1), new HexCoord(-1, 2, -1), new HexCoord(1, 1, -2)}
;       public static HexCoord Diagonal(int direction_)
        {
            return diagonal[direction_];
        }

        public static HexCoord operator +(HexCoord a, HexCoord b)
            => new HexCoord(a.q+b.q, a.r+b.r, a.s+b.s);
        public static HexCoord operator -(HexCoord a, HexCoord b)
            => new HexCoord(a.q-b.q, a.r-b.r, a.s-b.s);
       public static HexCoord operator *(HexCoord a, int k)
            => new HexCoord(a.q*k, a.r*k, a.s*k);
       public static HexCoord operator *(int k, HexCoord a)
            => a * k;
        public static bool operator ==(HexCoord a, HexCoord b)
        => a.q==b.q && a.r==b.r && a.s==b.s;
        public static bool operator !=(HexCoord a, HexCoord b)
        => !(a == b);

        public override int GetHashCode()
        {
            return HashCode.Combine(q, r);
        }

        public override bool Equals(object obj)
        {
            if (obj is HexCoord)
                return Equals((HexCoord) obj);
            else
                return false;
        }

        public bool Equals(HexCoord other)
        {
            return this == other;
        }

        public override string ToString ()
        {
            return base.ToString() + String.Format(": ({0}, {1}, {2})", q, r, s);
        }
        public HexCoord Scale(int k)
        {
            return this * k;
        }

        public HexCoord RotateLeft()
        {
            return new HexCoord(-s, -q, -r);
        }
        public HexCoord RotateRight()
        {
            return new HexCoord(-r, -s, -q);
        }
        public int Length()
        {
            return (Mathf.Abs(q) +  Mathf.Abs(r) + Mathf.Abs(s)) / 2;
        }

        public int Distance(HexCoord other)
        {
            return (this - other).Length();
        }
        public HexCoord Neighbor(int direction)
        {
            return this + Direction(direction);
        }

        public HexCoord DiagonalNeighbor(int direction)
        {
            return this + Diagonal(direction);
        }

        public Vector2 ToVertex(Layout layout)
        {
            ref HexOrientation M = ref layout.orientation;
            float x = (M.f0 * q + M.f1 * r) * layout.size.x;
            float y = (M.f2 * q + M.f3 * r) * layout.size.y;
            return new Vector2(x + layout.origin.x, y + layout.origin.y);
        }

        public HexCoord Lerp(HexCoord a, HexCoord b, float t)
        {
            return FractionalHexCoord.Lerp(a, b, t).Round();
        }

        public Vector3Int ToQOffset(int offset, int z = 0)
        {
            Assert.IsTrue( offset == Even || offset == Odd);
            int x = q;
            int y = r + ((q + offset * (q & 1)) / 2);
            return new Vector3Int(x, y, z);
        }
        public static HexCoord FromQOffset(int offset, Vector3Int h)
        {
            Assert.IsTrue( offset == Even || offset == Odd);
            int q = h.x;
            int r = h.y - ((h.x + offset * (h.x & 1)) / 2);
            int s = -q - r;
            return new HexCoord(q, r, s);
        }
        public Vector3Int ToROffset(int offset, int z = 0)
        {
            int x = q + ((r + offset * (r & 1)) / 2);
            int y = r;
            return new Vector3Int(x, y, z);
        }
        public static HexCoord FromROffset(int offset, Vector3Int h)
        {
            int q = h.x - ((h.y + offset * (h.y & 1)) / 2);
            int r = h.y;
            int s = -q - r;
            return new HexCoord(q, r, s);
        }
    }

    public struct FractionalHexCoord
    {
        public float q;
        public float r;
        public float s;

        static float Lerp(float a, float b, float t)
        {
            return a * (1-t) + b * t;
        }
        public FractionalHexCoord(float q_, float r_, float s_)
        {
            q = q_;
            r = r_;
            s = s_;
            Assert.IsTrue(Mathf.Abs(q + r + s) < 1.0e-6f);
        }

        public HexCoord Round()
        {
            int q_ = Mathf.RoundToInt(q);
            int r_ = Mathf.RoundToInt(r);
            int s_ = Mathf.RoundToInt(s);
            float dq = Mathf.Abs(q_ - q);
            float dr = Mathf.Abs(r_ - r);
            float ds = Mathf.Abs(s_ - s);
            if (dq > dr && dq > ds)
                q_ = -r_ - s_;
            else if (dr > ds)
                r_ = - q_ - s_;
            else
                s_ = -q_ - r_;
            return new HexCoord(q_, r_, s_);
        }

        public static FractionalHexCoord Lerp(HexCoord a, HexCoord b, float t)
        {
            return new FractionalHexCoord(Lerp(a.q, b.q, t),
                                          Lerp(a.r, b.r, t),
                                          Lerp(a.s, b.s, t));
        }
    }
    public struct HexOrientation
    {
        public float f0, f1, f2, f3;
        public float b0, b1, b2, b3;
        public float angle;
        public HexOrientation(float f0_, float f1_, float f2_, float f3_, 
                              float b0_, float b1_, float b2_, float b3_, 
                              float angle_)
        {
            f0 = f0_;
            f1 = f1_;
            f2 = f2_;
            f3 = f3_;
            b0 = b0_;
            b1 = b1_;
            b2 = b2_;
            b3 = b3_;
            angle = angle_;
        }
        public static HexOrientation pointy = 
            new HexOrientation(Mathf.Sqrt(3.0f), Mathf.Sqrt(3.0f) / 2.0f, 0.0f, 3.0f / 2.0f,
                                      Mathf.Sqrt(3.0f) / 3.0f, -1.0f / 3.0f, 0.0f, 2.0f / 3.0f,
                                      0.5f);
        public static HexOrientation flat =
            new HexOrientation(3.0f / 2.0f, 0.0f, Mathf.Sqrt(3.0f) / 2.0f, Mathf.Sqrt(3.0f),
                                      2.0f / 3.0f, 0.0f, -1.0f / 3.0f,  Mathf.Sqrt(3.0f) / 3.0f,
                                      0.0f);

        public static bool operator ==(HexOrientation h1, HexOrientation h2) 
        {
            return h1.angle == h2.angle;
        }
        public static bool operator !=(HexOrientation h1, HexOrientation h2) 
        {
            return h1.angle != h2.angle;
        }
        public override bool Equals(object obj)
        {
            if (obj is HexOrientation)
                return Equals((HexOrientation) obj);
            else
                return false;
        }        
        public bool Equals(HexOrientation other)
        {
            return this == other;
        }
        public override int GetHashCode()
        {
            return angle.GetHashCode();
        }
    }

    public struct Layout
    {
        public HexOrientation orientation;
        public Vector2 size;
        public Vector2 origin;

        public Layout(HexOrientation orientation_, Vector2 size_, Vector2 origin_)
        {
            orientation = orientation_;
            size = size_;
            origin = origin_;
        }

        public Vector2 HexToPixel(HexCoord h)
        {
            HexOrientation M = orientation;
            float x = (M.f0 * h.q + M.f1 * h.r) * size.x;
            float y = (M.f2 * h.q + M.f3 * h.r) * size.y;
            return new Vector2(x + origin.x, y + origin.y);
        }
        public FractionalHexCoord PixelToHex(Vector2 p)
        {
            HexOrientation M = orientation;
            Vector2 pt = new Vector2((p.x - origin.x) / size.x, (p.y - origin.y) / size.y);
            float q = M.b0 * pt.x + M.b1 * pt.y;
            float r = M.b2 * pt.x + M.b3 * pt.y;
            return new FractionalHexCoord(q, r, -q - r);
        }
    }
}