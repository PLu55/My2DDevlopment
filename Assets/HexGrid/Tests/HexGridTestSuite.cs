using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using HexGrid.Core;

public class HexGridTestScript
{
    // A Test behaves as an ordinary method
    [Test]
    public void HexEquality()
    {   
        HexCoord hg1 = HexCoord.Zero();
        HexCoord hg2 = new HexCoord(1, 0, -1);
        HexCoord hg3 = hg1;
        
        Assert.IsTrue(hg1.q == 0 && hg1.r == 0 && hg1.s == 0);
        Assert.IsTrue(hg1 == hg3);
        Assert.IsFalse(hg1 != hg3);
        Assert.IsTrue(hg1 != hg2);
        Assert.IsTrue(hg1.Equals(hg1));
        Assert.IsFalse(hg1.Equals(1));
    }

    [Test]
    public void HexArithmetic()
    {   
        Assert.AreEqual(new HexCoord(1, -3, 2) + new HexCoord(3, -7, 4), new HexCoord(4, -10, 6));
        Assert.AreEqual(new HexCoord(1, -3, 2) - new HexCoord(2, -7, 5), new HexCoord(-1, 4, -3));
        Assert.AreEqual(new HexCoord(1, -3, 2) * 3, new HexCoord(3, -9, 6));
        Assert.AreEqual(5 * new HexCoord(4, -7, 3), new HexCoord(20, -35, 15));
    }

    [Test]
    public void HexLengthAndDistance()
    {
        Assert.AreEqual(3 , new HexCoord(1, -3, 2).Length());
        Assert.AreEqual(7, new HexCoord(3, -7, 4).Distance(HexCoord.Zero()));
    }

 
    [Test]
    public void HexDirections()
    {
        Assert.AreEqual(new HexCoord(0, -1, 1) , HexCoord.Direction(2));
        Assert.AreEqual(new HexCoord(-2, 1, 1) , HexCoord.Diagonal(3));
    }
    
    [Test]
    public void HexNeighbor()
    {
        Assert.AreEqual(new HexCoord(3, -6, 3) , new HexCoord(4, -7, 3).Neighbor(4));
        Assert.AreEqual(new HexCoord(3, -5, 2) , new HexCoord(4, -7, 3).DiagonalNeighbor(4));
    }

    [Test]
    static public void Rotation()
    {
        Assert.AreEqual( new HexCoord(-3, -4, 7), new HexCoord(4, -7, 3).RotateLeft());
        Assert.AreEqual( new HexCoord(7, -3, -4), new HexCoord(4, -7, 3).RotateRight());
    }

    [Test]
    static public void TestLayout()
    {
        HexCoord h = new HexCoord(3, 4, -7);
        Layout flat = new Layout(HexOrientation.flat, new Vector2(10.0f, 15.0f), new Vector2(35.0f, 71.0f));
        Assert.AreEqual( h, flat.PixelToHex(flat.HexToPixel(h)).Round());
        Layout pointy = new Layout(HexOrientation.pointy, new Vector2(10.0f, 15.0f), new Vector2(35.0f, 71.0f));
        Assert.AreEqual(h, pointy.PixelToHex(pointy.HexToPixel(h)).Round());
    }

    [Test]
    static public void QOffsetCoord()
    {
        Assert.AreEqual(new HexCoord(-3, -4, 7),
            HexCoord.FromQOffset(HexCoord.Even, new HexCoord(-3, -4, 7).ToQOffset(HexCoord.Even)));
        Assert.AreEqual(new HexCoord(-3, -4, 7),
            HexCoord.FromQOffset(HexCoord.Odd, new HexCoord(-3, -4, 7).ToQOffset(HexCoord.Odd)));
    }

    [Test]
    static public void ROffsetCoord()
    {
        Assert.AreEqual(new HexCoord(-3, -4, 7),
            HexCoord.FromROffset(HexCoord.Even, new HexCoord(-3, -4, 7).ToROffset(HexCoord.Even)));
        Assert.AreEqual(new HexCoord(-3, -4, 7),
            HexCoord.FromROffset(HexCoord.Odd, new HexCoord(-3, -4, 7).ToROffset(HexCoord.Odd)));
    }
    
    // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
    // `yield return null;` to skip a frame.
    //[UnityTest]
    public IEnumerator NewTestScriptWithEnumeratorPasses()
    {
        // Use the Assert class to test conditions.
        // Use yield to skip a frame.
        yield return null;
    }
}
