using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using HexGrid.Core;
using ReGoap.Planner;

public class NavNode : INode<NavTile>
{
    NavNode parent;
    NavTile goal;
    NavTile tile;
    float gCost;
    float hCost;
    //HexCoord hexCoord;
    public int QueueIndex { get; set; }
    public float Priority { get; set; }

    public NavNode(NavTile tile_, NavTile goal_)
    {
        tile = tile_;
        parent = null;
        goal = goal_;
        gCost = 0;
        hCost = tile.hexCoord.Distance(goal.hexCoord);
    }
   public NavNode(NavNode parent_, NavTile tile_, NavTile goal_)
    {
        tile = tile_;
        parent = parent_;
        goal = goal_;
        gCost = tile_.cost + parent.gCost;
        hCost = tile.hexCoord.Distance(goal.hexCoord);
    }
    
    public override string ToString ()
    {
        return base.ToString() + String.Format(": pos: {0} g: {1} h: {2} f: {3}", tile.hexCoord, gCost, hCost, gCost + hCost);
    }
    public NavTile GetState() 
    { return tile; }

    // TODO: How to prevent tiles to be added more then once? 
    //       Check the direction to the parent is one idea but
    //       this seams to be complicated. A better ideais to
    //       keep record of already visited tiles. This is
    //       somethis teh A* should do in my oppinion.
    public List<INode<NavTile>> Expand()
    { 
        // Debug.LogFormat("Expand node: {0}", this);
        List<INode<NavTile>> neighbors = new List<INode<NavTile>>();
        for (int direction = 0; direction < 6; ++direction)
        {
            Vector3Int pos = tile.hexCoord.Neighbor(direction).ToROffset(HexCoord.Odd);
            if ( pos.x >= tile.tilemap.origin.x 
                 && pos.x < tile.tilemap.origin.x + tile.tilemap.size.x
                 && pos.y >= tile.tilemap.origin.y 
                 && pos.y < tile.tilemap.origin.y + tile.tilemap.size.y)
            {
                NavTile neighborTile = tile.tilemap.GetTile(pos) as NavTile;
                
                if (neighborTile != null && neighborTile.isNavigable)
                {
                    NavNode node = new NavNode(this, neighborTile, goal);
                    // Debug.LogFormat("add node {0}", node);
                    neighbors.Add(node);
                }
             }
        }

        //Debug.LogFormat("NavNode.Expand {0}", neighbors.Count);
        return neighbors;
    }
    public int CompareTo(INode<NavTile> other)
    {
        return GetCost().CompareTo(other.GetCost());
    }
    public float GetCost() // fCost ???
    { 
        return gCost + hCost;
    }
    public float GetHeuristicCost()
    {
        return hCost;
    }
    public float GetPathCost() // gCost ???
    {
        return gCost;
    } 
    public INode<NavTile> GetParent()
    {
        return parent;
    }
    public bool IsGoal(NavTile goal)
    {
        // Debug.LogFormat("IsGoal: {0} == {1} : {2}", this.tile.hexCoord, goal.hexCoord, this.tile == goal);
        return this.tile == goal;
        //return hCost == 0;
    }
    // TODO: Implement 
    public void Recycle()
    {

    }
}