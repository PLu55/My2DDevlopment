using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GameZone : MonoBehaviour
{
    public Tilemap tilemap;
    public Tile baseTile;
    private List<Tile> tiles;
    public List<string> tileNames;

    Tile[] tilesX;
    
    private void Start()
    {
        TileResourceLoader.LoadAll();
        //baseTile = TileResourceLoader.GetTileByName("hexBase");
        tileNames = TileResourceLoader.GetAllKeys();
        foreach(string s in TileResourceLoader.GetAllKeys())
            Debug.Log("TileName: " + s);
        SetupTiles();
    }
    private void SetupTiles()
    {
        // How to 
        // https://github.com/Unity-Technologies/2d-techdemos#2d-techdemos
        // https://github.com/Unity-Technologies/2d-extras
        // Tile _baseTile = TileResourceLoader.GetBaseTile();
        Tile tile;
        

        tilemap.ClearAllTiles();

        for(int i = -10; i < 10; ++i)
            for(int j = -10; j < 10; ++j)
            {
                Vector3Int pos = new Vector3Int(i,j, 0);
                int index = Random.Range(0,tileNames.Count);
                //tile = TileResourceLoader.GetTileByName(tileNames[index]);
                //Debug.Log("TileName: " + tileNames[index]);
                tile = Object.Instantiate(baseTile);
                //NavTile nTile = tile as NavTile;
                //if (nTile)
                //    nTile.tilemap = tilemap;
                tilemap.SetTile(pos, tile); 
            }
        Debug.LogFormat("Tilemap.size: {0} origin: {1}", tilemap.size, tilemap.origin);
        Debug.Log("Dummy: " + Dummy.jstr);
        //public static T Load(string path); 
    }

}
