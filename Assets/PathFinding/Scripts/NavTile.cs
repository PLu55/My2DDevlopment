using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using HexGrid.Core;
using ReGoap.Planner;

[CreateAssetMenu(fileName = "NavTile", menuName = "NavTile", order = 1)]
public class NavTile : Tile
{
    public bool isNavigable;
    public Tilemap tilemap;
    public HexOrientation orientation;
    public Vector2 size;
    public Vector2 origin;
    public float cost;
    public HexCoord hexCoord;

    public override bool StartUp(Vector3Int location, ITilemap iTilemap, GameObject go)
    {
        base.StartUp(location, iTilemap, go);
        hexCoord = HexCoord.FromROffset(HexCoord.Odd,location); // TODO: this is pointy top, generalize it
        tilemap = iTilemap.GetComponent<Tilemap>(); 
        return true;
    }

}
