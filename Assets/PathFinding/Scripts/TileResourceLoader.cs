using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public static class TileResourceLoader
{
    private const string BaseTile = "hexBase";
    private static Dictionary<string, Tile> tiles;

    public static Tile GetBaseTile()
    {
        return GetTileByName(BaseTile);
    }
    public static Tile GetTileByName(string name)
    {
        return tiles[name];
    }

    public static void LoadAll()
    {
        tiles = new Dictionary<string, Tile>();
        foreach (Tile t in Resources.FindObjectsOfTypeAll(typeof(Tile)))
        {
            tiles.TryAdd(t.name, t);
        }
    
        Dictionary<string, Tile>.KeyCollection keyColl = tiles.Keys;
  
        foreach(string s in keyColl)
        {
            Debug.Log("Key: " + s);
        }
    }

    public static List<string> GetAllKeys()
    {
        List<string> keys = new List<string>();
        foreach(string s in tiles.Keys)
        {
            keys.Add(s);
        }
        return keys;
    }
}
 