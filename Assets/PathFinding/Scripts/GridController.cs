using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UIElements;
using ReGoap.Planner;
using HexGrid.Core;

public class GridController : MonoBehaviour
{

    private Grid grid;
    [SerializeField] private Tilemap interactiveMap = null;
    [SerializeField] private Tilemap pathMap = null;
    [SerializeField] private Tilemap hoverMap = null;
    [SerializeField] private Tile hoverTile = null;
    [SerializeField] private Tile pathTile = null;
    [SerializeField] private Tile posTile = null;
    private Vector3Int previousMousePos = new Vector3Int();
    private bool drawMode;
    private Vector3Int startPos;
    private Vector3Int goalPos;
    private bool[] mouseButtons = new bool[2];
    void Start() 
    {
        grid = gameObject.GetComponent<Grid>();
        drawMode = false;
        startPos = Vector3Int.zero;
        goalPos = Vector3Int.zero;
        interactiveMap.SetTile(startPos, posTile);
    }
    void Update() 
    {
        // Mouse over -> highlight tile
        Vector3Int mousePos = GetMousePosition();


        if (!mousePos.Equals(previousMousePos)) 
        {
            hoverMap.SetTile(previousMousePos, null); // Remove old hoverTile
            HexCoord hc = HexCoord.FromROffset(HexCoord.Odd, mousePos);
            //Debug.LogFormat("mousePos offset: {0} {1} {2}", mousePos, hc, hc.ToROffset(HexCoord.Odd)==mousePos);
            //mousePos.z = 1;
            hoverMap.SetTile(mousePos, hoverTile);
            previousMousePos = mousePos;
        }


        // Left mouse click -> add path tile


        if (drawMode && Input.GetMouseButtonDown(0) )
        {
            pathMap.SetTile(mousePos, pathTile);
        }
        if (!drawMode && Input.GetMouseButtonDown(0) )
        {
            INode<NavTile> path = null;

            interactiveMap.ClearAllTiles();
            interactiveMap.SetTile(mousePos, posTile);
            goalPos = mousePos;
            NavTile goal = pathMap.GetTile(goalPos) as NavTile;
            NavTile start = pathMap.GetTile(startPos) as NavTile;
            interactiveMap.SetTile(goalPos, posTile);

            if (start != null && goal != null)
                path = FindPath(start, goal);
            else
                Debug.Log("Star and/or goal is not set!" + start + goal);
            if (path != null)
            {
                TraversePath(path);
            }
            startPos = goalPos;
        }

        // Right mouse click -> remove path tile

        if (!drawMode && Input.GetMouseButtonDown(1)) 
        {
            pathMap.SetTile(mousePos, null);
        }
        if (false && !drawMode && Input.GetMouseButtonDown(1)) 
        {
            NavTile tile = pathMap.GetTile(mousePos) as NavTile;
            if (tile != null)
                Debug.LogFormat("{0} {1}", tile, tile.hexCoord );
        }
    }

    Vector3Int GetMousePosition () {

        Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        return grid.WorldToCell(mouseWorldPos);

    }
    public void OnToggleChanged(bool val)
    {
        drawMode = val;
    }
    public INode<NavTile> FindPath(NavTile start, NavTile goal)
    {
        Debug.LogFormat("FindPath( {0}, {1}", start.hexCoord, goal.hexCoord);
        return PathFinder.FindPath(start, goal);
    }

    public void TraversePath(INode<NavTile> goalNode)
    {
        INode<NavTile> node = goalNode;
        while( node != null)
        {
            NavTile tile = node.GetState();
            interactiveMap.SetTile(tile.hexCoord.ToROffset(HexCoord.Odd), posTile);
            //Debug.LogFormat("Coord: {0}", tile.hexCoord);
            node = node.GetParent();
       }
    }
}
