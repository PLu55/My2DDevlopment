using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HexGrid.Core;
using ReGoap.Planner;

public class PathFinder
{
    
    private static AStar<NavTile> aStar = new AStar<NavTile>();

    public static INode<NavTile> FindPath(NavTile start, NavTile goal)
    {
        NavNode startNode = new NavNode(start, goal);
        INode<NavTile> path = aStar.Run(startNode, goal, 1000);
        return path;
    }
    
}
