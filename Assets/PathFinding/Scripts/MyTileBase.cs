using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/MyTileBase", order = 1)]
public class MyTileBase : TileBase
{
    // Must Implement Tile!
    public Sprite sprite;
    public Color color;
    Matrix4x4 transform;
    public GameObject gameObject;
    public TileFlags flags;
    public Tile.ColliderType colliderType;
    public override void GetTileData(Vector3Int location, ITilemap tilemap, ref TileData tileData)
    {
        tileData.sprite = this.sprite;
        tileData.color = this.color;
        tileData.transform = this.transform;
        tileData.gameObject = this.gameObject;
        tileData.flags = this.flags;
        tileData.colliderType = this.colliderType;
    }
    
    //public override bool GetTileAnimationData(Vector3Int location, ITilemap tilemap, ref TileAnimationData tileAnimationData)
    //{ 
    //    return false;
    //}

    //public void RefreshTile(Vector3Int location, ITilemap tilemap)
    //public bool StartUp(Vector3Int location, ITilemap tilemap, GameObject go)
}
