using UnityEngine;

[System.Serializable]
public class GameZoneInfo
{

    public string name;
    public ZoneTile[] tiles;

    GameZoneInfo(string _name, ZoneTile[] _tiles)
    {
        name = _name;
        tiles = _tiles;
    }
}

[System.Serializable]
public class ZoneTile
{
    public string name;
    public float probability;

    ZoneTile(string _name, float _probability)
    {
        name = _name;
        probability = _probability;
    }

} 

public static class Dummy
{
    //public static ZoneTile olle = JsonUtility.FromJson<ZoneTile>(jstr);
    //public Dummy() {}
    public static TextAsset jstr = Resources.Load<TextAsset>("Zone001");

}