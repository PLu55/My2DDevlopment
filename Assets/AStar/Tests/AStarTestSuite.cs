//using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using ReGoap.Planner;

public class TestNode : INode<int>
{
    TestNode parent;

    int id;
    float gCost;
    float hCost;

    public TestNode(int i, float g, float h)
    {
        parent = null;
        id = i;
        gCost = g;
        hCost = h;

    }
   public TestNode(TestNode parent_, int i, float g, float h)
    {
        parent = parent_;
        id = i;
        gCost = g;
        hCost = h;

    }
    public int GetState()
     {
        return id;
     }

    public List<INode<int>> Expand()
    {
        return new List<INode<int>>();
    }

    public int CompareTo(INode<int> other)
    {
        if (this.GetCost() > other.GetCost())
            return 1;
        else if (this.GetCost() < other.GetCost())
            return -1;
        return 0;
    }

    public float GetCost()
    {
        return gCost + hCost;
    }
    public float GetHeuristicCost()
    {
        return hCost;
    }
    public float GetPathCost()
    { 
        return gCost;
    }
    public INode<int> GetParent()
    {
        return parent;
    } 
    public bool IsGoal(int goal)
    {
        return goal == 0;
    }
    public int QueueIndex { get; set; }
    public float Priority { get; set; }
    public void Recycle(){}
}

public class AStarTestSuit
{
    int n = 1000;
    FastPriorityQueue<TestNode, int> q;
    List<TestNode> nodes;
    public void GenerateNodes()
    {
        q = new FastPriorityQueue<TestNode, int>(n);
        nodes = new List<TestNode>();
        TestNode parent_ = null;

        for (int i = 0; i < n; ++i)
        {
            float g = Random.Range(0, 10);
            float h = Random.Range(0, 10);
            TestNode node = new TestNode(parent_, i, g, h);
            parent_ = node;
            nodes.Add(node);
            q.Enqueue(node, Random.Range(0.0f, (float) n));
        }
    }

    [Test]
    public void FastPriorityQueueEnqueueDequeueTests()
    { 
        GenerateNodes();
        Assert.IsTrue(q.IsValidQueue());
        Assert.IsTrue(q.Count == n);

        for (int i = n-1; i > 0; --i)
        {
            float p = 0.0f;
            TestNode node = q.Dequeue();
            Assert.IsTrue(node.Priority >= p);
            p = node.Priority;
            Assert.IsTrue(q.Count == i);
        }
    }
    [Test]
    public void FastPriorityQueueContainsRemoveTests()
    {   
        GenerateNodes();
        Assert.IsTrue(q.Count == n);
        for (int i = n-1; i > 0; --i)
        {
            int idx = Random.Range(0, i);
            TestNode node = nodes[idx];
            nodes.Remove(node);
            Assert.IsTrue(q.Contains(node));
            q.Remove(node);
            Assert.IsFalse(q.Contains(node));
            Assert.IsTrue(q.Count == i);
        }
    }
}
